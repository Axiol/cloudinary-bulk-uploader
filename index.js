const cloudinary = require('./cloudinary')
const fs = require('fs');
const localFolder = './bulk/';

fs.readdirSync(localFolder).forEach(file => {
  const uploader = async (path) => await cloudinary.uploads(path, process.env.DESTINATION_FOLDER);
  const newPath = uploader('./bulk/' + file);
});